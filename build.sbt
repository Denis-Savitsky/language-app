name := "languageApp"

version := "0.1"

scalaVersion := "2.13.5"

idePackagePrefix := Some("ru.tinkoff.denis")

enablePlugins(SbtLiquibase)

lazy val circeVersion = "0.13.0"
lazy val AkkaVersion = "2.6.13"
lazy val slickVersion = "3.3.3"

import com.permutive.sbtliquibase.SbtLiquibase

liquibaseUsername := "postgres"
liquibasePassword := "12121996"
liquibaseDriver   := "org.postgresql.Driver"
liquibaseUrl      := "jdbc:postgresql://localhost:5432/lang_app"
liquibaseDefaultSchemaName := Some("public")

ThisBuild / libraryDependencies ++= Seq(
  "com.softwaremill.sttp.tapir" %% "tapir-akka-http-server" % "0.18.0-M4",
  // slick
  "com.typesafe.slick" %% "slick" % slickVersion,
  "org.slf4j" % "slf4j-nop" % "1.6.4",
  "com.typesafe.slick" %% "slick-hikaricp" % slickVersion,
  "com.h2database" % "h2" % "1.4.200",
  "com.github.t3hnar" %% "scala-bcrypt" % "4.3.0",

  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2",
  "de.heikoseeberger" %% "akka-http-circe" % "1.36.0",
  "com.typesafe.akka" %% "akka-http-spray-json" % "10.2.4",
  "io.spray" %% "spray-json" % "1.3.6",
  "io.circe" %% "circe-core" % circeVersion,
  "io.circe" %% "circe-generic" % circeVersion,
  "io.circe" %% "circe-parser" % circeVersion,
  "org.mongodb.scala" %% "mongo-scala-driver" % "4.2.3",
  "org.flywaydb" % "flyway-core" % "7.8.2",
  "org.postgresql" % "postgresql" % "42.2.20",
  "org.reactivemongo" %% "reactivemongo" % "1.0.3",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe.akka" %% "akka-slf4j" % AkkaVersion,
  "io.netty" % "netty-all" % "4.1.63.Final",

  "com.typesafe.akka" %% "akka-stream-testkit" % "2.6.13" % Test,
  "com.typesafe.akka" %% "akka-http-testkit" % "10.2.4" % Test,
  "org.scalamock" %% "scalamock" % "4.4.0" % Test,
  "org.scalatest" %% "scalatest" % "3.2.2" % Test,
)


