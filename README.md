# Language app

This is an application server for language learning app completed as a course work for Tinkoff Scala School.

# Technologies

- Akka Http - Server
- MongoDB (Mongo reactive driver) - used for storing appointments data and comments
- PostgresQL - used to store different information about tutors and students accounts
- LiquiBase - used for database schema migration
- Slick - used for FRM
- Circe - used for serializing-desializing messages


# Role model
- Tutors
- Students
- Admin


