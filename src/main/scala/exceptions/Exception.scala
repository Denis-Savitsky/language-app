package ru.tinkoff.denis
package exceptions

sealed abstract class AppException(message: String) extends Exception(message)

final case class NotEnoughMoneyOnAccountException(name: String) extends AppException(s"User $name has not enough money on the account")

final case class UnauthorizedAccessException(login: String) extends AppException(s"Password for user $login is invalid")

final case class TutorNotFoundException(tutorName: String) extends AppException(s"Tutor with name $tutorName not found")

final case class StudentNotFoundException(studentName: String) extends AppException(s"Student $studentName not found")

final case class LoginOccupiedException(login: String) extends AppException(s"User with login $login already exists")

final case class AppointmentNotExistsException(login: String, time: String) extends AppException(s"Appointment for user $login on time $time does not exist")