package ru.tinkoff.denis
package exceptions

import akka.http.scaladsl.model.StatusCodes.BadRequest
import akka.http.scaladsl.server.Directives.complete
import akka.http.scaladsl.server.ExceptionHandler

object AppExceptionHandler {

  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

  val exceptionHandler: ExceptionHandler =
    ExceptionHandler {
      case e: AppException => complete(BadRequest, ExceptionResponse(e.getMessage))
    }
}
