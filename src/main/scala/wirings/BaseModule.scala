package ru.tinkoff.denis
package wirings

import admin.service.{AdminAuthorizationService, AdminAuthorizationServiceImpl, AdminService, AdminServiceImpl}
import appointments.mongo.AppointmentQueryRepositoryImpl
import appointments.service.{AppointmentService, AppointmentServiceImpl}
import bcrypt.{AsyncBcrypt, AsyncBcryptImpl}
import billing.rdbms.{AccountModel, AccountQueryRepositoryImpl}
import billing.service.{BillingService, BillingServiceImpl}
import cost.{CostService, CostServiceImpl}
import login.rdbms.{LoginQueryRepositoryImpl, UserRecordModel}
import login.service.{LoginService, LoginServiceImpl}
import student.rdbms.{StudentModel, StudentQueryRepositoryImpl}
import student.service.{StudentService, StudentServiceImpl}
import tutor.rdbms.{TutorModel, TutorQueryRepositoryImpl}
import tutor.service.{TutorService, TutorServiceImpl}

import akka.actor.ActorSystem
import com.typesafe.config.{Config, ConfigFactory}
import reactivemongo.api.MongoConnection.ParsedURI
import reactivemongo.api.{AsyncDriver, DB, MongoConnection}
import comment.mongo.CommentQueryRepositoryImpl
import comment.service.{CommentService, CommentServiceImpl}
import slick.jdbc.JdbcBackend.Database
import slick.jdbc.PostgresProfile

import scala.concurrent.{ExecutionContext, Future}


object BaseModule {

  implicit val ac: ActorSystem = ActorSystem()
  implicit val ec: ExecutionContext = ac.dispatcher

  private val config: Config = ConfigFactory.load()
  private val mongoUri: String = config.getString("mongo.url")
  private val driver: AsyncDriver = AsyncDriver()
  private val parsedUri: Future[ParsedURI] = MongoConnection.fromString(mongoUri)
  private val futureConnection: Future[MongoConnection] = parsedUri.flatMap(c =>driver.connect(c))
  private def dbApp: Future[DB] = futureConnection.flatMap(_.database("lang-app"))

  private val db = Database.forConfig("mydb")
  private val profile = PostgresProfile

  private val accountModel = new AccountModel(profile)
  private val studentModel = new StudentModel(profile)
  private val tutorModel = new TutorModel(profile)
  private val userRecordModel = new UserRecordModel(profile)

  private val appointmentQueryRepository = new AppointmentQueryRepositoryImpl(dbApp)
  private val accountQueryRepository = new AccountQueryRepositoryImpl(accountModel, db)
  private val studentQueryRepository = new StudentQueryRepositoryImpl(studentModel, db)
  private val tutorQueryRepository = new TutorQueryRepositoryImpl(tutorModel, db)
  private val loginQueryRepository = new LoginQueryRepositoryImpl(userRecordModel, db)
  private val commentQueryRepository = new CommentQueryRepositoryImpl(dbApp)


  private[wirings] val bcrypt: AsyncBcrypt = new AsyncBcryptImpl
  private[wirings] val appointmentService: AppointmentService = new AppointmentServiceImpl(appointmentQueryRepository)
  private[wirings] val billingService: BillingService = new BillingServiceImpl(accountQueryRepository)
  private[wirings] val loginService: LoginService = new LoginServiceImpl(bcrypt, loginQueryRepository)
  private[wirings] val costService: CostService = new CostServiceImpl
  private[wirings] val commentService: CommentService = new CommentServiceImpl(commentQueryRepository)

  private[wirings] val studentService: StudentService = new StudentServiceImpl(appointmentService, billingService, bcrypt, costService, studentQueryRepository,
    tutorQueryRepository, loginService, commentService)
  private[wirings] val tutorService: TutorService = new TutorServiceImpl(bcrypt, billingService, appointmentService, costService, tutorQueryRepository, loginService)
  private[wirings] val adminService: AdminService = new AdminServiceImpl(appointmentService, studentService, commentService, tutorService, loginService, bcrypt)
  private[wirings] val adminAuthorizationService: AdminAuthorizationService = new AdminAuthorizationServiceImpl(loginService)

}
