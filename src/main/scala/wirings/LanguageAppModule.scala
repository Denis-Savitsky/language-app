package ru.tinkoff.denis
package wirings


import student.api.StudentApi
import tutor.api.TutorApi

import akka.http.scaladsl.server.{Route, RouteConcatenation}
import com.typesafe.config.{Config, ConfigFactory}
import ru.tinkoff.denis.exceptions.AppExceptionHandler


object LanguageAppModule {

  import BaseModule._

  val tutorRoute: TutorApi = new TutorApi(tutorService, loginService)
  val studentRoute: StudentApi = new StudentApi(studentService, loginService)

  val routes: Route = Route.seal(
    RouteConcatenation.concat(
      tutorRoute.route,
      studentRoute.route,
    )
  )(exceptionHandler = AppExceptionHandler.exceptionHandler)

  val config: Config = ConfigFactory.load()
  val port: Int = config.getInt("languageApp.port")
  val interface: String = config.getString("languageApp.interface")


}
