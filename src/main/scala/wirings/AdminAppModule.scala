package ru.tinkoff.denis
package wirings

import admin.service.{AdminAuthorizationServiceImpl, AdminService, AdminServiceImpl}

import akka.http.scaladsl.server.Route
import com.typesafe.config.{Config, ConfigFactory}
import ru.tinkoff.denis.admin.api.AdminApi
import ru.tinkoff.denis.exceptions.AppExceptionHandler


object AdminAppModule {

  import BaseModule._


  private val adminService: AdminService = new AdminServiceImpl(appointmentService, studentService, commentService, tutorService, loginService, bcrypt)
  private val adminAuthorizationService = new AdminAuthorizationServiceImpl(loginService)

  val adminRoute: AdminApi = new AdminApi(adminService, adminAuthorizationService)

  val routes: Route = Route.seal(adminRoute.route)(exceptionHandler = AppExceptionHandler.exceptionHandler)

  val config: Config = ConfigFactory.load()
  val port: Int = config.getInt("languageApp-admin.port")
  val interface: String = config.getString("languageApp-admin.interface")

}
