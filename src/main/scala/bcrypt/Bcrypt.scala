package ru.tinkoff.denis
package bcrypt

import com.github.t3hnar.bcrypt.BCryptStrOps

import scala.concurrent.{ExecutionContext, Future}

trait AsyncBcrypt {

  def hash(password: String, rounds: Int = 12): Future[String]

  def verify(password: String, hash: String): Future[Boolean]


}

class AsyncBcryptImpl(implicit executionContext: ExecutionContext) extends AsyncBcrypt {
  override def hash(password: String, rounds: Int): Future[String] =
    Future {
      password.bcryptBounded(rounds)
    }

  override def verify(password: String, hash: String): Future[Boolean] =
    Future {
      password.isBcryptedBounded(hash)
    }
}
