package ru.tinkoff.denis
package appointments.service

import appointments.mongo.AppointmentQueryRepository
import appointments.model.Appointment

import scala.concurrent.Future

class AppointmentServiceImpl(appointmentQueryRepository: AppointmentQueryRepository) extends AppointmentService {

  override def createNewAppointment(studentName: String, tutorName: String, time: String): Future[Unit] = {
    val appointment = Appointment(tutorName, studentName, time)
    appointmentQueryRepository.createAppointment(appointment)
  }

  override def cancelAppointmentOfTutor(tutorName: String, time: String): Future[Option[Appointment]] = {
    appointmentQueryRepository.cancelAppointmentOfTutor(tutorName, time)
  }

  override def cancelAppointmentOfStudent(studentName: String, time: String): Future[Option[Appointment]] = {
    appointmentQueryRepository.cancelAppointmentOfStudent(studentName, time)
  }

  override def deleteAllAppointmentsOfStudent(studentName: String): Future[Option[Appointment]] = {
    appointmentQueryRepository.deleteAllAppointmentsOfStudent(studentName)
  }

  override def findAllAppointments: Future[List[Appointment]] =
    appointmentQueryRepository.findAllAppointments

  override def findAppointmentsOfTutor(tutorName: String): Future[List[Appointment]] =
    appointmentQueryRepository.findAppointmentsByTutorName(tutorName)

  override def findAppointmentsOfStudent(studentName: String): Future[List[Appointment]] =
    appointmentQueryRepository.findAppointmentsByStudentName(studentName)

  override def cancelAppointment(tutorName: String, studentName: String, time: String): Future[Option[Appointment]] =
    appointmentQueryRepository.cancelAppointment(tutorName, studentName, time)
}
