package ru.tinkoff.denis
package appointments.service

import appointments.model.Appointment

import scala.concurrent.Future

trait AppointmentService {
  def createNewAppointment(studentName: String, tutorName: String, time: String): Future[Unit]
  def cancelAppointmentOfTutor(tutorName: String, time: String): Future[Option[Appointment]]
  def cancelAppointmentOfStudent(studentName: String, time: String): Future[Option[Appointment]]
  def cancelAppointment(tutorName: String, studentName: String, time: String): Future[Option[Appointment]]
  def deleteAllAppointmentsOfStudent(studentName: String): Future[Option[Appointment]]
  def findAllAppointments: Future[List[Appointment]]
  def findAppointmentsOfTutor(tutorName: String): Future[List[Appointment]]
  def findAppointmentsOfStudent(studentName: String): Future[List[Appointment]]
}
