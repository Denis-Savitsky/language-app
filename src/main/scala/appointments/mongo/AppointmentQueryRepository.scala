package ru.tinkoff.denis
package appointments.mongo

import appointments.model.Appointment

import reactivemongo.api.Cursor
import reactivemongo.api.bson.collection.BSONCollection
import reactivemongo.api.bson.{BSONDocumentReader, BSONDocumentWriter, Macros, document}

import scala.concurrent.Future

trait AppointmentQueryRepository {
  def createAppointment(appointment: Appointment): Future[Unit]
  def updateAppointment(appointment: Appointment): Future[Int]
  def findAllAppointments: Future[List[Appointment]]
  def findAppointmentsByTutorName(tutorName: String): Future[List[Appointment]]
  def findAppointmentsByStudentName(studentName: String): Future[List[Appointment]]
  def deleteAllAppointmentsOfStudent(studentName: String): Future[Option[Appointment]]
  def cancelAppointment(tutorName: String, studentName: String, time: String): Future[Option[Appointment]]
  def cancelAppointmentOfStudent(studentName: String, time: String): Future[Option[Appointment]]
  def cancelAppointmentOfTutor(tutorName: String, time: String): Future[Option[Appointment]]

}
