package ru.tinkoff.denis
package appointments.mongo

import reactivemongo.api.{AsyncDriver, Cursor, DB, MongoConnection}
import reactivemongo.api.bson.{BSONDocumentReader, BSONDocumentWriter, Macros, document}
import appointments.model.Appointment

import com.typesafe.config.{Config, ConfigFactory}
import reactivemongo.api.MongoConnection.ParsedURI
import reactivemongo.api.bson.collection.BSONCollection

import scala.concurrent.{ExecutionContext, Future}

class AppointmentQueryRepositoryImpl(val dbApp: Future[DB])(implicit ec: ExecutionContext) extends AppointmentQueryRepository {

  private val appointmentCollection: Future[BSONCollection] = dbApp.map(_.collection("appointments"))

  implicit def appointmentWriter: BSONDocumentWriter[Appointment] = Macros.writer[Appointment]

  override def createAppointment(appointment: Appointment): Future[Unit] =
    appointmentCollection.flatMap(_.insert.one(appointment).map(_ => {}))

  override def updateAppointment(appointment: Appointment): Future[Int] = {
    val selector = document(
      "tutorName" -> appointment.tutorName,
      "studentName" -> appointment.studentName,
    )

    appointmentCollection.flatMap(_.update.one(selector, appointment).map(_.n))
  }

  implicit def personReader: BSONDocumentReader[Appointment] = Macros.reader[Appointment]

  override def findAllAppointments: Future[List[Appointment]] = {
    appointmentCollection.flatMap(_.find(document()).
      cursor[Appointment]().
      collect[List](-1, Cursor.FailOnError[List[Appointment]]()))
  }

  override def findAppointmentsByTutorName(tutorName: String): Future[List[Appointment]] = {
    appointmentCollection.flatMap(_.find(document("tutorName" -> tutorName)).
      cursor[Appointment]().
      collect[List](-1, Cursor.FailOnError[List[Appointment]]()))
  }

  override def findAppointmentsByStudentName(studentName: String): Future[List[Appointment]] = {
    appointmentCollection.flatMap(_.find(document("studentName" -> studentName)).
      cursor[Appointment]().
      collect[List](-1, Cursor.FailOnError[List[Appointment]]()))
  }

  override def deleteAllAppointmentsOfStudent(studentName: String): Future[Option[Appointment]] =
    appointmentCollection.flatMap(_.findAndRemove(document("studentName" -> studentName)).map(_.result[Appointment]))

  override def cancelAppointment(tutorName: String, studentName: String, time: String): Future[Option[Appointment]] =
    appointmentCollection
      .flatMap(_.findAndRemove(document("tutorName" -> tutorName, "time" -> time, "studentName" -> studentName))
      .map(_.result[Appointment]))

  override def cancelAppointmentOfStudent(studentName: String, time: String): Future[Option[Appointment]] =
    appointmentCollection.flatMap(_.findAndRemove(document("studentName" -> studentName, "time" -> time)).map(_.result[Appointment]))

  override def cancelAppointmentOfTutor(tutorName: String, time: String): Future[Option[Appointment]] =
    appointmentCollection.flatMap(_.findAndRemove(document("tutorName" -> tutorName, "time" -> time)).map(_.result[Appointment]))
}
