package ru.tinkoff.denis
package appointments.model

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

case class Appointment(tutorName: String, studentName: String, time: String)

object Appointment {
  implicit val jsonDecoder: Decoder[Appointment] = deriveDecoder
  implicit val jsonEncoder: Encoder[Appointment] = deriveEncoder
}