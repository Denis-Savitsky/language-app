package ru.tinkoff.denis
package cost

import ru.tinkoff.denis.tutor.model.Tutor

import scala.concurrent.{ExecutionContext, Future}

class CostServiceImpl(implicit ec: ExecutionContext) extends CostService {
  def calculateCostOfLesson(tutor: Tutor): Future[Int] = {
    Future {
      tutor.experienceYears * 100 * (if (tutor.isNative) 2 else 1)
    }

  }
}
