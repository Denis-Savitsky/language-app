package ru.tinkoff.denis
package cost

import tutor.model.Tutor

import scala.concurrent.Future

trait CostService {
  def calculateCostOfLesson(tutor: Tutor): Future[Int]

}
