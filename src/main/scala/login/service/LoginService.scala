package ru.tinkoff.denis
package login.service

import ru.tinkoff.denis.login.model.UserRecord

import scala.concurrent.Future

trait LoginService {
  def verifyCredentials(login: String, password: String): Future[Boolean]

  def checkIfLoginExists(login: String): Future[Boolean]

  def deleteLoginInfo(login: String): Future[Int]

  def addLoginInfo(record: UserRecord): Future[Int]

  def updateAccountEmail(login: String, email: String): Future[Int]

  def updateAccountPassword(login: String, password: String): Future[Int]
}