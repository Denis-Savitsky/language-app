package ru.tinkoff.denis
package login.service

import bcrypt.AsyncBcrypt
import login.model.UserRecord
import login.rdbms.LoginQueryRepository

import scala.concurrent.{ExecutionContext, Future}



class LoginServiceImpl(bcrypt: AsyncBcrypt, loginQueryRepository: LoginQueryRepository)(implicit ec: ExecutionContext) extends LoginService {

  override def verifyCredentials(login: String, password: String): Future[Boolean] = {
    loginQueryRepository.findHashedPassword(login).flatMap {
      case Some(value) => bcrypt.verify(password, value)
      case _ => Future.successful(false)
    }
  }

  override def checkIfLoginExists(login: String): Future[Boolean] =
    loginQueryRepository.findLogin(login).flatMap {
      case Some(_) => Future.successful(true)
      case _ => Future.successful(false)
    }

  override def addLoginInfo(record: UserRecord): Future[Int] =
    loginQueryRepository.addLogin(record)

  override def deleteLoginInfo(login: String): Future[Int] =
    loginQueryRepository.deleteLoginInfo(login)

  override def updateAccountEmail(login: String, email: String): Future[Int] =
    loginQueryRepository.updateEmail(login, email)

  override def updateAccountPassword(login: String, password: String): Future[Int] =
    loginQueryRepository.updatePassword(login, password)

}
