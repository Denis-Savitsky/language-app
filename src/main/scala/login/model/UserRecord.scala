package ru.tinkoff.denis
package login.model

case class UserRecord(id: Int,
                      login: String,
                      hashedPassword: String,
                      name: String,
                      email: String)
