package ru.tinkoff.denis
package login.rdbms

import login.model.UserRecord

import scala.concurrent.Future

trait LoginQueryRepository {
  def addLogin(record: UserRecord): Future[Int]
  def findHashedPassword(login: String): Future[Option[String]]
  def findLogin(login: String): Future[Option[String]]
  def deleteLoginInfo(login: String): Future[Int]
  def updatePassword(login: String, password: String): Future[Int]
  def updateEmail(login: String, eMail: String): Future[Int]
}
