package ru.tinkoff.denis
package login.rdbms

import login.model.UserRecord

import slick.jdbc.{JdbcBackend, JdbcProfile}

import scala.concurrent.Future

class LoginQueryRepositoryImpl(userRecordModel: UserRecordModel, db: JdbcBackend#Database) extends LoginQueryRepository {
  import userRecordModel.AllRecords
  import userRecordModel.dbProfile.api._


  override def addLogin(record: UserRecord): Future[Int] =
    db.run(AllRecords returning AllRecords.map(_.loginId) += record)

  override def findHashedPassword(login: String): Future[Option[String]] =
    db.run(
      AllRecords.filter(_.login === login)
        .map(_.hashedPassword)
        .result
        .headOption
    )

  override def findLogin(login: String): Future[Option[String]] =
    db.run(
      AllRecords.filter(_.login === login)
        .map(_.login)
        .result
        .headOption
    )

  override def deleteLoginInfo(login: String): Future[Int] =
    db.run(
      AllRecords
        .filter(_.login === login)
        .delete
    )

  override def updatePassword(login: String, password: String): Future[Int] =
    db.run(
      AllRecords
        .filter(_.login === login)
        .map(_.hashedPassword)
        .update(password)
    )

  override def updateEmail(login: String, eMail: String): Future[Int] =
    db.run(
      AllRecords
        .filter(_.login === login)
        .map(_.email)
        .update(eMail)
    )

}