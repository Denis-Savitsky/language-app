package ru.tinkoff.denis
package login.rdbms

import ru.tinkoff.denis.login.model.UserRecord
import slick.jdbc.JdbcProfile
import slick.lifted.ProvenShape


class UserRecordModel(val dbProfile: JdbcProfile) {
  import dbProfile.api._

  class UserRecordsTable(tag: Tag) extends Table[UserRecord](tag, "user_records") {
    def loginId: Rep[Int] = column("id", O.PrimaryKey, O.AutoInc)
    def login: Rep[String] = column("login", O.Unique)
    def hashedPassword: Rep[String] = column("password")
    def name: Rep[String] = column("name")
    def email: Rep[String] = column("email", O.Unique)

    override def * : ProvenShape[UserRecord] = (loginId, login, hashedPassword, name, email).mapTo[UserRecord]
  }

  val AllRecords = TableQuery[UserRecordsTable]
}



