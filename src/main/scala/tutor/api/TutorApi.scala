package ru.tinkoff.denis
package tutor.api

import exceptions.LoginOccupiedException
import login.service.LoginService
import tutor.dto.TutorDTO
import tutor.service._

import akka.http.scaladsl.server.Route
import io.circe.generic.codec.DerivedAsObjectCodec.deriveCodec
import tutor.dto.TutorDTO.tutorDto2Tutor

import scala.concurrent.{ExecutionContext, Future}


class TutorApi(tutorService: TutorService, loginService: LoginService)(implicit val ec: ExecutionContext) {

  import akka.http.scaladsl.server.Directives._
  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

  def route: Route = routeNewTutor ~ allAppointments ~ deleteAccount ~ cancelAppointment ~ routeUpdatePassword ~ routeUpdateEmail

  val routeNewTutor: Route =
    (path("tutors") & parameter("password")) { password =>
      post {
        entity(as[TutorDTO]) { tutor =>
          complete(loginService.checkIfLoginExists(tutor.nickname).map {
            case false => tutorService.createTutor(tutorDto2Tutor(tutor), password)
            case _ => Future.failed(LoginOccupiedException(tutor.nickname))
          })
        }
      }
    }

  val deleteAccount: Route =
    (path("tutors") & parameters("login", "password")) { (login, password) =>
      authorizeAsync(loginService.verifyCredentials(login, password)) {
        delete {
          complete(tutorService.deleteTutor(login))
        }
      }
    }

  val cancelAppointment: Route =
    (path("tutors" / "appointment" / "cancel") & parameters("login", "password", "tutorName", "time")) {
      (login, password, tutorName, time) =>
        authorizeAsync(loginService.verifyCredentials(login, password)) {
          get {
            complete(tutorService.cancelAppointment(login, tutorName, time))
          }
        }
    }

  val allAppointments: Route =
    (path("tutors" / "appointment") & parameters("login", "password")) {
      (login, password) =>
        authorizeAsync(loginService.verifyCredentials(login, password)) {
          get {
            complete(tutorService.getListOfAppointments(login))
          }
        }
    }

  val routeUpdatePassword: Route =
    (path("tutors" / "update" / "password") & parameters("login", "password", "newPassword")) { (login, password, newPassword) =>
      authorizeAsync(loginService.verifyCredentials(login, password)) {
        post {
          complete(tutorService.updatePassword(login, newPassword))
        }
      }
    }

  val routeUpdateEmail: Route =
    (path("tutors" / "update" / "email") & parameters("login", "password", "newEmail")) { (login, password, newEmail) =>
      authorizeAsync(loginService.verifyCredentials(login, password)) {
        post {
          complete(tutorService.updateEmail(login, newEmail))
        }
      }
    }
}
