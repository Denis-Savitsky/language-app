package ru.tinkoff.denis
package tutor.service

import tutor.rdbms._
import bcrypt._
import billing.service.BillingService
import tutor.model.Tutor

import ru.tinkoff.denis.appointments.model.Appointment
import ru.tinkoff.denis.appointments.service.AppointmentService
import ru.tinkoff.denis.cost.CostService
import ru.tinkoff.denis.exceptions.{AppointmentNotExistsException, StudentNotFoundException, TutorNotFoundException}
import ru.tinkoff.denis.login.model.UserRecord
import ru.tinkoff.denis.login.service.LoginService

import scala.concurrent.{ExecutionContext, Future}


class TutorServiceImpl(bcrypt: AsyncBcrypt, billingService: BillingService, appointmentService: AppointmentService, costService: CostService,
                       tutorQueryRepository: TutorQueryRepository, loginService: LoginService)(implicit ec: ExecutionContext) extends TutorService {

  override def createTutor(tutor: Tutor, password: String): Future[Int] = {
    bcrypt.hash(password).flatMap {
      password =>
        tutorQueryRepository.addTutor(tutor) flatMap { _ =>
            val login = UserRecord(-1, tutor.nickname, password, tutor.name, tutor.mail)
            loginService.addLoginInfo(login)
              .flatMap(_ => billingService.createNewAccount(tutor.nickname, tutor.name))
        }
    }
  }

  override def updateNickName(oldName: String, newName: String): Future[Boolean] = {
    val tutor = tutorQueryRepository.findTutorByName(oldName)
    tutor.flatMap {
        case Some(tutor) => Future {
          tutorQueryRepository.updateNickname(tutor.id, newName)
          true
        }
        case _ => Future.successful(false)
    }
  }

  override def deleteTutor(login: String): Future[Int] = {
    tutorQueryRepository.deleteTutor(login).flatMap(_ =>
      loginService.deleteLoginInfo(login).flatMap { _ =>
        billingService.deleteAccount(login)
      }
    )
  }

  override def cancelAppointment(login: String, tutorName: String, time: String): Future[Int] =
    tutorQueryRepository.findTutorByLogin(login).flatMap {
      case Some(tutor) =>
        appointmentService.cancelAppointmentOfTutor(tutor.name, time).flatMap {
          case Some(appointment) => costService.calculateCostOfLesson(tutor).flatMap {
              cost => billingService.transferMoney(tutorName, appointment.studentName, cost)
          }
          case _ => throw AppointmentNotExistsException(tutorName, time)
        }
      case _ => throw TutorNotFoundException(login)
    }

  override def updatePassword(login: String, newPassword: String): Future[Int] =
    bcrypt.hash(newPassword).flatMap {
      loginService.updateAccountPassword(login, _)
    }

  override def updateEmail(login: String, eMail: String): Future[Int] =
    loginService.updateAccountEmail(login, eMail)

  override def getListOfAppointments(login: String): Future[List[Appointment]] =
    tutorQueryRepository.findTutorByLogin(login).flatMap {
      case Some(tutor) => appointmentService.findAppointmentsOfTutor(tutor.name)
      case _ => throw TutorNotFoundException(login)
    }



  override def getTutors: Future[Seq[Tutor]] = tutorQueryRepository.getAllTutors

  override def deleteTutorByName(name: String): Future[Int] =
    tutorQueryRepository.deleteTutorByName(name)
}
