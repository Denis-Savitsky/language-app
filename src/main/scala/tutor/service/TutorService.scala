package ru.tinkoff.denis
package tutor.service

import tutor.model.Tutor

import ru.tinkoff.denis.appointments.model.Appointment

import scala.concurrent.Future

trait TutorService {
  def createTutor(tutor: Tutor, password: String): Future[Int]
  def updateNickName(oldName: String, newName: String): Future[Boolean]
  def deleteTutor(login: String): Future[Int]
  def deleteTutorByName(name: String): Future[Int]
  def getTutors: Future[Seq[Tutor]]
  def updateEmail(login: String, eMail: String): Future[Int]
  def updatePassword(login: String, newPassword: String): Future[Int]
  def getListOfAppointments(login: String): Future[List[Appointment]]
  def cancelAppointment(login: String, tutorName: String, time: String): Future[Int]

}
