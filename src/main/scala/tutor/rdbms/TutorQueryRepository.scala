package ru.tinkoff.denis
package tutor.rdbms

import tutor.model.Tutor

import scala.concurrent.Future

trait TutorQueryRepository {
  def findTutor(id: Int): Future[Option[Tutor]]
  def findTutorByName(name: String): Future[Option[Tutor]]
  def findTutorByLogin(login: String): Future[Option[Tutor]]
  def addTutor(tutor: Tutor): Future[Int]
  def updateNickname(tutorId: Int, nickname: String): Future[Int]
  def deleteTutor(login: String): Future[Int]
  def deleteTutorByName(name: String): Future[Int]
  def getAllTutors: Future[Seq[Tutor]]
}
