package ru.tinkoff.denis
package tutor.rdbms

import tutor.model.Tutor

import slick.jdbc.JdbcProfile
import slick.lifted.ProvenShape


class TutorModel(val dbProfile: JdbcProfile) {
  import dbProfile.api._

  class TutorsTable(tag: Tag) extends Table[Tutor](tag,"tutors") {

    def tutorId: Rep[Int] = column("id", O.PrimaryKey, O.AutoInc)

    def name: Rep[String] = column("name")

    def language: Rep[String] = column("language")

    def nickname: Rep[String] = column("nickname")

    def mail: Rep[String] = column("mail")

    def experienceYears: Rep[Int] = column("experience_years")

    def isNative: Rep[Boolean] = column("native")

    override def * : ProvenShape[Tutor] = (tutorId, name, language, nickname, mail, experienceYears, isNative).mapTo[Tutor]
  }
  val AllTutors = TableQuery[TutorsTable]
}



