package ru.tinkoff.denis
package tutor.rdbms

import tutor.model.Tutor

import slick.jdbc.{JdbcBackend, JdbcProfile}

import scala.concurrent.Future

class TutorQueryRepositoryImpl(tutorModel: TutorModel, db: JdbcBackend#Database) extends TutorQueryRepository {
  import tutorModel.AllTutors
  import tutorModel.dbProfile.api._


  override def findTutor(id: Int): Future[Option[Tutor]] =
    db.run(
      AllTutors
        .filter(_.tutorId === id)
        .result
        .headOption
    )

  override def findTutorByName(name: String): Future[Option[Tutor]] = {
    db.run(
      AllTutors
        .filter(_.name === name)
        .result
        .headOption
    )
  }

  override def findTutorByLogin(login: String): Future[Option[Tutor]] = {
    db.run(
      AllTutors
        .filter(_.nickname === login)
        .result
        .headOption
    )
  }

  override def addTutor(tutor: Tutor): Future[Int] =
    db.run(
      (AllTutors returning AllTutors.map(_.tutorId)) += tutor
    )

  override def updateNickname(tutorId: Int, nickname: String): Future[Int] =
    db.run(AllTutors.filter(_.tutorId === tutorId).map(_.nickname).update(nickname))

  override def deleteTutor(login: String): Future[Int] =
    db.run(AllTutors
      .filter(_.nickname === login)
      .delete)

  override def deleteTutorByName(name: String): Future[Int] =
    db.run(AllTutors
      .filter(_.name === name)
      .delete)

  override def getAllTutors: Future[Seq[Tutor]] =
    db.run(AllTutors.result)
}