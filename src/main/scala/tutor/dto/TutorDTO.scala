package ru.tinkoff.denis
package tutor.dto

import tutor.model.Tutor

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}
import ru.tinkoff.denis.student.dto.StudentDTO
import ru.tinkoff.denis.student.model.Student

import scala.language.implicitConversions

case class TutorDTO(name: String,
               language: String,
               nickname: String,
               mail: String,
               experienceYears: Int,
               isNative: Boolean = false)

object TutorDTO {
  implicit val jsonDecoder: Decoder[Tutor] = deriveDecoder
  implicit val jsonEncoder: Encoder[Tutor] = deriveEncoder

  def tutorDto2Tutor(tutorDto: TutorDTO): Tutor =
    Tutor(-1, tutorDto.name, tutorDto.language, tutorDto.nickname, tutorDto.mail, tutorDto.experienceYears, tutorDto.isNative)

}