package ru.tinkoff.denis
package tutor.model


case class Tutor(id: Int,
                 name: String,
                 language: String,
                 nickname: String,
                 mail: String,
                 experienceYears: Int,
                 isNative: Boolean = false)
