package ru.tinkoff.denis
package student.dto

import student.model.Student

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

import scala.language.implicitConversions

case class StudentDTO(name: String,
                      mail: String,
                      nickname: String,
                      level: String)

object StudentDTO {
  implicit val jsonDecoder: Decoder[StudentDTO] = deriveDecoder
  implicit val jsonEncoder: Encoder[StudentDTO] = deriveEncoder

  def studentDto2Student(studentDto: StudentDTO): Student =
    Student(-1, studentDto.name, studentDto.mail, studentDto.nickname, studentDto.level)
}