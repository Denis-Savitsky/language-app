package ru.tinkoff.denis
package student.service

import student.model.Student

import ru.tinkoff.denis.appointments.model.Appointment
import ru.tinkoff.denis.comment.model.Comment

import scala.concurrent.Future

trait StudentService {
  def createStudent(student: Student, password: String): Future[Int]
  def deleteStudent(studentName: String): Future[Int]
  def getStudents: Future[Seq[Student]]
  def getStudentNameById(id: Int): Future[String]
  def depositBalance(login: String, sum: Int): Future[Int]
  def makeAppointment(login: String, tutorName: String, time: String): Future[Int]
  def cancelAppointment(login: String, time: String): Future[Int]
  def updatePassword(login: String, newPassword: String): Future[Int]
  def updateEmail(login: String, newPassword: String): Future[Int]
  def getListOfAppointments(login: String): Future[List[Appointment]]
  def createCommentForTutor(comment: Comment): Future[Unit]
  def getAllCommentsForTutor(tutorName: String): Future[List[Comment]]
  def getTutorRating(tutorName: String): Future[Double]

}
