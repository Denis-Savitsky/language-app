package ru.tinkoff.denis
package student.service

import student.rdbms._
import bcrypt._
import appointments.service._
import billing.service._
import cost.CostService
import exceptions.{AppointmentNotExistsException, StudentNotFoundException, TutorNotFoundException}
import student.model.Student
import tutor.rdbms.TutorQueryRepository

import ru.tinkoff.denis.appointments.model.Appointment
import ru.tinkoff.denis.comment.model.Comment
import ru.tinkoff.denis.comment.service.CommentService
import ru.tinkoff.denis.login.model.UserRecord
import ru.tinkoff.denis.login.service.LoginService

import scala.concurrent.{ExecutionContext, Future}

class StudentServiceImpl(appointmentService: AppointmentService, billingService: BillingService, bcrypt: AsyncBcrypt, costService: CostService,
                         studentQueryRepository: StudentQueryRepository, tutorQueryRepository: TutorQueryRepository,
                         loginService: LoginService, commentService: CommentService)(implicit ec: ExecutionContext) extends StudentService {

  override def createStudent(student: Student, password: String): Future[Int] = {
    bcrypt.hash(password).flatMap {
      hashedPassword =>
        studentQueryRepository.addStudent(student).flatMap { _ =>
            val login = UserRecord(-1, student.nickname, hashedPassword, student.name, student.mail)
            loginService.addLoginInfo(login)
              .flatMap(_ => billingService.createNewAccount(student.nickname, student.name))
        }
    }
  }

  override def deleteStudent(login: String): Future[Int] = {
    studentQueryRepository.deleteStudent(login).flatMap(_ =>
      loginService.deleteLoginInfo(login).flatMap { _ =>
        billingService.deleteAccount(login)
      }
    )
  }

  override def getStudents: Future[Seq[Student]] = studentQueryRepository.getAllStudents

  override def getStudentNameById(id: Int): Future[String] = studentQueryRepository.findStudent(id).map {
    case Some(student) => student.name
    case _ => throw StudentNotFoundException("No student found")
  }

  override def depositBalance(login: String, sum: Int): Future[Int] = {
    billingService.updateAccount(login, sum)
  }

  override def makeAppointment(login: String, tutorName: String, time: String): Future[Int] =
    studentQueryRepository.findStudentByLogin(login).flatMap {
      case Some(student) => {
        appointmentService.createNewAppointment(student.name, tutorName, time).flatMap { _ =>
          tutorQueryRepository.findTutorByName(tutorName).flatMap {
            case Some(tutor) => costService.calculateCostOfLesson(tutor).flatMap {
              cost => billingService.transferMoney(student.name, tutorName, cost)
            }
            case _ => Future.failed(TutorNotFoundException(tutorName))
          }
        }
      }
      case _ => Future.failed(StudentNotFoundException(login))
    }

  override def cancelAppointment(login: String, time: String): Future[Int] =
    studentQueryRepository.findStudentByLogin(login).flatMap {
      case Some(student) =>
        appointmentService.cancelAppointmentOfStudent(student.name, time).flatMap {
          case Some(appointment) =>
          tutorQueryRepository.findTutorByName(appointment.tutorName).flatMap {
            case Some(tutor) => costService.calculateCostOfLesson(tutor).flatMap {
              cost => billingService.transferMoney(appointment.tutorName, student.name, cost)
            }
            case _ => Future.failed(TutorNotFoundException(appointment.tutorName))
          }
          case _ => Future.failed(AppointmentNotExistsException(student.name, time))
        }
      case _ => Future.failed(StudentNotFoundException(login))
    }

  override def updatePassword(login: String, newPassword: String): Future[Int] =
    bcrypt.hash(newPassword).flatMap {
      loginService.updateAccountPassword(login, _)
    }

  override def updateEmail(login: String, eMail: String): Future[Int] =
    loginService.updateAccountEmail(login, eMail)

  override def getListOfAppointments(login: String): Future[List[Appointment]] =
    studentQueryRepository.findStudentByLogin(login).flatMap {
      case Some(student) => appointmentService.findAppointmentsOfStudent(student.name)
      case _ => Future.failed(StudentNotFoundException(login))
    }

  override def createCommentForTutor(comment: Comment): Future[Unit] =
    tutorQueryRepository.findTutorByName(comment.tutorName).map {
      case Some(_) => commentService.createComment(comment)
      case _ => Future.failed(TutorNotFoundException(comment.tutorName))
    }

  override def getAllCommentsForTutor(tutorName: String): Future[List[Comment]] =
    tutorQueryRepository.findTutorByName(tutorName).flatMap {
      case Some(_) => commentService.findCommentsByTutorName(tutorName)
      case _ =>  Future.failed(TutorNotFoundException(tutorName))
    }

  override def getTutorRating(tutorName: String): Future[Double] =
    tutorQueryRepository.findTutorByName(tutorName).flatMap {
      case Some(_) => commentService.getTutorAverageRating(tutorName)
      case _ => Future.failed(TutorNotFoundException(tutorName))
    }
}
