package ru.tinkoff.denis
package student.model

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

case class Student(id: Int,
                   name: String,
                   mail: String,
                   nickname: String,
                   level: String)


