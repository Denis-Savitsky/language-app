package ru.tinkoff.denis
package student.rdbms

import student.model.Student

import scala.concurrent.Future

trait StudentQueryRepository {
  def findStudent(id: Int): Future[Option[Student]]
  def findStudentByLogin(login: String): Future[Option[Student]]
  def findStudentByName(name: String): Future[Option[Student]]
  def addStudent(student: Student): Future[Int]
  def deleteStudent(login: String): Future[Int]
  def getAllStudents: Future[Seq[Student]]
}
