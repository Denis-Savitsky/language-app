package ru.tinkoff.denis
package student.rdbms

import student.model.Student

import slick.jdbc.{JdbcBackend, JdbcProfile}

import scala.concurrent.Future

class StudentQueryRepositoryImpl(studentModel: StudentModel, db: JdbcBackend#Database) extends StudentQueryRepository {
  import studentModel.AllStudents
  import studentModel.dbProfile.api._

  override def findStudent(id: Int): Future[Option[Student]] = {
    val action = AllStudents
      .filter(_.studentId === id)
      .result
      .headOption

    db.run(action)
  }

  override def findStudentByLogin(login: String): Future[Option[Student]] = {
    val action = AllStudents
      .filter(_.nickname === login)
      .result
      .headOption

    db.run(action)
  }

  override def findStudentByName(name: String): Future[Option[Student]] = {
    val action = AllStudents
      .filter(_.name === name)
      .result
      .headOption

    db.run(action)
  }

  override def addStudent(student: Student): Future[Int] =
    db.run(AllStudents  += student)

  override def deleteStudent(login: String): Future[Int] = {
    val action = AllStudents
      .filter(_.nickname === login)
      .delete

    db.run(action)
  }

  override def getAllStudents: Future[Seq[Student]] = {
    db.run(AllStudents.result)
  }
}
