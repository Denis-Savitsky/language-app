package ru.tinkoff.denis
package student.rdbms

import student.model.Student

import slick.jdbc.JdbcProfile
import slick.lifted.ProvenShape


class StudentModel(val dbProfile: JdbcProfile) {
  import dbProfile.api._

  class StudentsTable(tag: Tag) extends Table[Student](tag, "students") {

    def studentId: Rep[Int] = column("id", O.PrimaryKey, O.AutoInc)

    def name: Rep[String] = column("name")

    def mail: Rep[String] = column("mail")

    def nickname: Rep[String] = column("nickname")

    def level: Rep[String] = column("language_level")

    override def * : ProvenShape[Student] = (studentId, name, mail, nickname, level).mapTo[Student]
  }

  val AllStudents = TableQuery[StudentsTable]

}



