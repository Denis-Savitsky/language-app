package ru.tinkoff.denis
package student.api

import exceptions.LoginOccupiedException
import login.service.LoginService
import student.dto.StudentDTO
import student.service._

import akka.http.scaladsl.server.Route
import io.circe.generic.codec.DerivedAsObjectCodec.deriveCodec
import ru.tinkoff.denis.comment.model.Comment
import ru.tinkoff.denis.student.dto.StudentDTO.studentDto2Student

import scala.concurrent.{ExecutionContext, Future}


class StudentApi(studentService: StudentService, loginService: LoginService)(implicit ec: ExecutionContext) {

  import akka.http.scaladsl.server.Directives._
  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

  def route: Route = routeNewStudent ~ deleteAccount ~ routeMakeAppointment ~ routeUpdatePassword ~ routeUpdateEmail ~ cancelAppointment ~ depositBalance ~ allAppointments ~ createComment ~ getTutorComments ~ getTutorRatings

  val routeNewStudent: Route =
    (path("students") & parameter("password")) { password =>
      post {
        entity(as[StudentDTO]) { student =>
          complete(loginService.checkIfLoginExists(student.nickname).map {
            case false => studentService.createStudent(studentDto2Student(student), password)
            case _ => Future.failed(LoginOccupiedException(student.nickname))
          })
        }
      }
    }

  val deleteAccount: Route =
    (path("students") & parameters("login", "password")) { (login, password) =>
      authorizeAsync(_ => loginService.verifyCredentials(login, password)) {
        delete {
          complete(studentService.deleteStudent(login))
        }
      }
    }

  val routeUpdatePassword: Route =
    (path("students" / "update" / "password") & parameters("login", "password", "newPassword")) { (login, password, newPassword) =>
      authorizeAsync(_ => loginService.verifyCredentials(login, password)) {
        post {
          complete(studentService.updatePassword(login, newPassword))
        }
      }
    }

  val routeUpdateEmail: Route =
    (path("students" / "update" / "email") & parameters("login", "password", "newEmail")) { (login, password, newEmail) =>
      authorizeAsync(_ => loginService.verifyCredentials(login, password)) {
        post {
          complete(studentService.updateEmail(login, newEmail))
        }
      }
    }

  val routeMakeAppointment: Route =
    (path("students" / "appointment") & parameters("login", "password", "tutorName", "time")) {
      (login, password, tutorName, time) =>
        authorizeAsync(_ => loginService.verifyCredentials(login, password)) {
          post {
            complete(studentService.makeAppointment(login, tutorName, time))
          }
        }
    }

  val cancelAppointment: Route =
    (path("students" / "appointment" / "cancel") & parameters("login", "password", "time")) {
      (login, password, time) =>
        authorizeAsync(_ => loginService.verifyCredentials(login, password)) {
          post {
            complete(studentService.cancelAppointment(login, time))
          }
        }
    }

  val allAppointments: Route =
    (path("students" / "appointment") & parameters("login", "password")) {
      (login, password) =>
        authorizeAsync(_ => loginService.verifyCredentials(login, password)) {
          get {
            complete(studentService.getListOfAppointments(login))
          }
        }
    }

  val depositBalance: Route =
    (path("students" / "balance") & parameters("login", "password", "sum".as[Int])) {
      (login, password, sum) =>
        authorizeAsync(_ => loginService.verifyCredentials(login, password)) {
          post {
            complete(studentService.depositBalance(login, sum))
          }
        }
    }

  val createComment: Route =
    (path("students" / "comments") & parameters("login", "password")) {
      (login, password) =>
        authorizeAsync(_ => loginService.verifyCredentials(login, password)) {
          post {
            entity(as[Comment]) { comment =>
              complete(studentService.createCommentForTutor(comment))
            }
          }
        }
    }

  val getTutorComments: Route =
    (path("students" / "comments" / "tutorComments") & parameters("tutorName", "login", "password")) {
      (tutorName, login, password) =>
        authorizeAsync(_ => loginService.verifyCredentials(login, password)) {
          get {
            complete(studentService.getAllCommentsForTutor(tutorName))
          }
        }
    }

  val getTutorRatings: Route =
    (path("students" / "rating") & parameters("login", "password", "tutorName")) {
      (login, password, tutorName) =>
        authorizeAsync(_ => loginService.verifyCredentials(login, password)) {
          get {
            complete(studentService.getTutorRating(tutorName))
          }
        }
    }



}
