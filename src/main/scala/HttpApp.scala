package ru.tinkoff.denis


import akka.http.scaladsl.Http
import com.typesafe.scalalogging.LazyLogging

import scala.concurrent.Future

import wirings.BaseModule._

object LanguageHttpApp {

  def main(args: Array[String]): Unit = {
    LanguageApp().start()
    AdminApp().start()
  }
}

case class LanguageApp() extends LazyLogging {

  import wirings.LanguageAppModule._

  def start(): Future[Http.ServerBinding] =
    Http()
      .newServerAt(interface, port)
      .bind(routes)
      .andThen { case b => logger.info(s"language server started at: $b") }
}

case class AdminApp() extends LazyLogging {

  import wirings.AdminAppModule._

  def start(): Future[Http.ServerBinding] =
    Http()
      .newServerAt(interface, port)
      .bind(routes)
      .andThen { case b => logger.info(s"admin server started at: $b") }
}