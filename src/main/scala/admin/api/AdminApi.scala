package ru.tinkoff.denis
package admin.api

import admin.service.{AdminAuthorizationService, AdminService}

import akka.http.scaladsl.server.Route

import scala.concurrent.ExecutionContext


class AdminApi(adminService: AdminService, adminAuthorizationService: AdminAuthorizationService)(implicit ec: ExecutionContext) {

  import akka.http.scaladsl.server.Directives._
  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

  def route: Route = routeCancel ~ routeCancelByStudent ~ routeCancelByTutor ~ routeBanStudent ~ routeDeleteTutor ~ routeShowAllAppointments ~ routeShowAllAppointmentsOfTutor ~ routeShowAllAppointmentsOfStudent ~ routeGetAllComments ~ changePassword

  val changePassword: Route =
    (path("admin" / "password") & parameters("old", "new")) {
      (pass, newPass) =>
        authorizeAsync(_ => adminAuthorizationService.authorize(pass)) {
          post {
            complete(adminService.changeAdminPassword(newPass))
          }
        }
    }

  val routeCancel: Route =
    (path("admin" / "cancel") & parameters("pass", "studentName", "tutorName", "time")) {
      (pass, studentName, tutorName, time) =>
        authorizeAsync(_ => adminAuthorizationService.authorize(pass)) {
          post {
            complete(adminService.cancelAppointment(studentName, tutorName, time))
          }
        }
    }

  val routeCancelByStudent: Route =
    (path("admin" / "cancel") & parameters("pass", "studentName", "time")) {
      (pass, studentName, time) =>
        authorizeAsync(_ => adminAuthorizationService.authorize(pass)) {
          post {
            complete(
              adminService.cancelAppointmentOfStudent(studentName, time))
          }
        }
    }

  val routeCancelByTutor: Route =
    (path("admin" / "cancel") & parameters("pass", "tutorName", "time")) {
      (pass, tutorName, time) =>
        authorizeAsync(_ => adminAuthorizationService.authorize(pass)) {
          post {
            complete(
              adminService.cancelAppointmentOfTutor(tutorName, time))
          }
        }
    }

  val routeBanStudent: Route =
    (path("admin" / "delete" / "student" / Remaining) & parameter("pass")) {
      (name, pass) =>
        authorizeAsync(_ => adminAuthorizationService.authorize(pass)) {
          delete {
            complete(adminService.deleteStudent(name))
          }
        }
    }

  val routeDeleteTutor: Route =
    (path("admin" / "delete" / "tutor") & parameters("name", "pass")) {
      (name, pass) =>
        authorizeAsync(_ => adminAuthorizationService.authorize(pass)) {
          delete {
            complete(
              adminService.deleteTutor(name))
          }
        }

    }

  val routeShowAllAppointments: Route =
    (path("admin" / "appointments") & parameter("pass")) { pass =>
      authorizeAsync(_ => adminAuthorizationService.authorize(pass)) {
        get {
          complete(adminService.listAllAppointments)
        }
      }
    }

  val routeShowAllAppointmentsOfStudent: Route =
    (path("admin" / "appointments" / "students") & parameters("studentName", "pass")) { (studentName, pass) =>
      authorizeAsync(_ => adminAuthorizationService.authorize(pass)) {
        get {
          complete(
            adminService.listAppointmentsOfStudent(studentName)
          )
        }
      }
    }

  val routeShowAllAppointmentsOfTutor: Route =
    (path("admin" / "appointments" / "tutors") & parameter("tutorName", "pass")) {
      (tutorName, pass) =>
        authorizeAsync(_ => adminAuthorizationService.authorize(pass)) {
          get {
            complete(
              adminService.listAppointmentsOfTutor(tutorName)
            )
          }
        }
    }

  val routeGetAllComments: Route =
    (path("admin" / "comments") & parameter("pass")) { pass =>
      authorizeAsync(_ => adminAuthorizationService.authorize(pass)) {
        get {
          complete(
            adminService.getAllComments
          )
        }
      }
    }

}
