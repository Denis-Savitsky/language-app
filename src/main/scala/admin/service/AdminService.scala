package ru.tinkoff.denis
package admin.service

import student.model.Student
import tutor.model.Tutor

import ru.tinkoff.denis.appointments.model.Appointment
import ru.tinkoff.denis.comment.model.Comment

import scala.concurrent.Future

trait AdminService {
  def cancelAppointment(studentName: String, tutorName: String, time: String): Future[Option[Appointment]]
  def cancelAppointmentOfTutor(tutorName: String, time: String): Future[Option[Appointment]]
  def cancelAppointmentOfStudent(studentName: String, time: String): Future[Option[Appointment]]
  def deleteStudent(studentName: String): Unit
  def deleteTutor(tutorName: String): Unit
  def listAllUsers: Future[Seq[Student]]
  def listAllTutors: Future[Seq[Tutor]]
  def listAllAppointments: Future[List[Appointment]]
  def listAppointmentsOfStudent(studentName: String): Future[List[Appointment]]
  def listAppointmentsOfTutor(tutorName: String): Future[List[Appointment]]
  def changeAdminPassword(newPassword: String): Future[Int]
  def changeAdminEmail(newEmail: String): Future[Int]
  def getAllComments: Future[List[Comment]]
}
