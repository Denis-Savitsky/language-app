package ru.tinkoff.denis
package admin.service

import scala.concurrent.Future

trait AdminAuthorizationService {
  def authorize(pass: String): Future[Boolean]
}
