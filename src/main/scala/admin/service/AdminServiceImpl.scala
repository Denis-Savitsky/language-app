package ru.tinkoff.denis
package admin.service

import student.model.Student
import tutor.model.Tutor

import ru.tinkoff.denis.appointments.model.Appointment
import ru.tinkoff.denis.appointments.service.AppointmentService
import ru.tinkoff.denis.bcrypt.AsyncBcrypt
import ru.tinkoff.denis.comment.model.Comment
import ru.tinkoff.denis.comment.service.CommentService
import ru.tinkoff.denis.login.rdbms.LoginQueryRepository
import ru.tinkoff.denis.login.service.LoginService
import ru.tinkoff.denis.student.service.StudentService
import ru.tinkoff.denis.tutor.service.TutorService

import scala.concurrent.{ExecutionContext, Future}



class AdminServiceImpl(appointmentService: AppointmentService, studentService: StudentService, commentService: CommentService,
                       tutorService: TutorService, loginService: LoginService, bcrypt: AsyncBcrypt)(implicit ec: ExecutionContext) extends AdminService {

  override def cancelAppointment(studentName: String, tutorName: String, time: String): Future[Option[Appointment]] =
    appointmentService.cancelAppointment(tutorName, studentName, time)

  override def cancelAppointmentOfTutor(tutorName: String, time: String): Future[Option[Appointment]] =
    appointmentService.cancelAppointmentOfTutor(tutorName, time)

  override def cancelAppointmentOfStudent(studentName: String, time: String): Future[Option[Appointment]] =
    appointmentService.cancelAppointmentOfStudent(studentName, time)

  override def deleteStudent(studentName: String): Unit =
    studentService.deleteStudent(studentName)

  override def deleteTutor(tutorName: String): Unit =
    tutorService.deleteTutorByName(tutorName)

  override def listAllUsers: Future[Seq[Student]] =
    studentService.getStudents

  override def listAllTutors: Future[Seq[Tutor]] =
    tutorService.getTutors

  override def listAllAppointments: Future[List[Appointment]] =
    appointmentService.findAllAppointments

  override def listAppointmentsOfStudent(studentName: String): Future[List[Appointment]] =
    appointmentService.findAppointmentsOfStudent(studentName)

  override def listAppointmentsOfTutor(tutorName: String): Future[List[Appointment]] =
    appointmentService.findAppointmentsOfTutor(tutorName)

  override def changeAdminPassword(newPassword: String): Future[Int] = {
    bcrypt.hash(newPassword).flatMap {
      loginService.updateAccountPassword("admin", _)
    }

  }

  override def changeAdminEmail(newEmail: String): Future[Int] =
    loginService.updateAccountEmail("admin", newEmail)

  override def getAllComments: Future[List[Comment]] =
    commentService.listAllComments
}

