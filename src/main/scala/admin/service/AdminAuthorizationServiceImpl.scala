package ru.tinkoff.denis
package admin.service

import ru.tinkoff.denis.login.service.LoginService

import scala.concurrent.{ExecutionContext, Future}

class AdminAuthorizationServiceImpl(loginService: LoginService)(implicit ec: ExecutionContext) extends AdminAuthorizationService {

  override def authorize(pass: String): Future[Boolean] =
    loginService.verifyCredentials("admin", pass)
}
