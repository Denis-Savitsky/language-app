package ru.tinkoff.denis
package comment.service
import comment.model.Comment

import ru.tinkoff.denis.comment.mongo.CommentQueryRepository

import scala.concurrent.Future

class CommentServiceImpl(val commentQueryRepository: CommentQueryRepository) extends CommentService {

  override def createComment(comment: Comment): Future[Unit] =
    commentQueryRepository.createComment(comment)

  override def listAllComments: Future[List[Comment]] =
    commentQueryRepository.findAllComments

  override def findAppointmentsByAuthor(authorName: String): Future[List[Comment]] =
    commentQueryRepository.findAppointmentsByAuthor(authorName)

  override def findCommentsByTutorName(tutorName: String): Future[List[Comment]] =
    commentQueryRepository.findCommentsByTutorName(tutorName)

  override def getTutorAverageRating(tutorName: String): Future[Double] =
    commentQueryRepository.getTutorAverageRating(tutorName)
}
