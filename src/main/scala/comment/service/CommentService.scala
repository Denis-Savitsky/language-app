package ru.tinkoff.denis
package comment.service

import comment.model.Comment

import scala.concurrent.Future

trait CommentService {
  def createComment(comment: Comment): Future[Unit]
  def listAllComments: Future[List[Comment]]
  def findAppointmentsByAuthor(authorName: String): Future[List[Comment]]
  def findCommentsByTutorName(tutorName: String): Future[List[Comment]]
  def getTutorAverageRating(tutorName: String): Future[Double]
}
