package ru.tinkoff.denis
package comment.model

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

case class Comment(tutorName: String, comment: String, rating: Int, author: String)

object Comment {
  implicit val jsonDecoder: Decoder[Comment] = deriveDecoder
  implicit val jsonEncoder: Encoder[Comment] = deriveEncoder
}


