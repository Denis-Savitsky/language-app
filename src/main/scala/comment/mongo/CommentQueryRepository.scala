package ru.tinkoff.denis
package comment.mongo

import ru.tinkoff.denis.comment.model.Comment

import scala.concurrent.Future

trait CommentQueryRepository {
  def createComment(comment: Comment): Future[Unit]
  def findAllComments: Future[List[Comment]]
  def findAppointmentsByAuthor(authorName: String): Future[List[Comment]]
  def findCommentsByTutorName(tutorName: String): Future[List[Comment]]
  def getTutorAverageRating(tutorName: String): Future[Double]
}
