package ru.tinkoff.denis
package comment.mongo


import reactivemongo.api.bson.collection.BSONCollection
import reactivemongo.api.bson.{BSONDocumentReader, BSONDocumentWriter, Macros, document}
import reactivemongo.api.{Cursor, DB}
import ru.tinkoff.denis.comment.model.Comment

import scala.concurrent.{ExecutionContext, Future}

class CommentQueryRepositoryImpl(val dbApp: Future[DB])(implicit ec: ExecutionContext) extends CommentQueryRepository {

  private val commentCollection: Future[BSONCollection] = dbApp.map(_.collection("comments"))

  implicit def appointmentWriter: BSONDocumentWriter[Comment] = Macros.writer[Comment]

  override def createComment(comment: Comment): Future[Unit] =
    commentCollection.flatMap(_.insert.one(comment).map(_ => {}))

  implicit def personReader: BSONDocumentReader[Comment] = Macros.reader[Comment]

  override def findAllComments: Future[List[Comment]] = {
    commentCollection.flatMap(_.find(document()).
      cursor[Comment]().
      collect[List](-1, Cursor.FailOnError[List[Comment]]()))
  }

  override def findCommentsByTutorName(tutorName: String): Future[List[Comment]] = {
    commentCollection.flatMap(_.find(document("tutorName" -> tutorName)).
      cursor[Comment]().
      collect[List](-1, Cursor.FailOnError[List[Comment]]()))
  }

  override def findAppointmentsByAuthor(authorName: String): Future[List[Comment]] = {
    commentCollection.flatMap(_.find(document("author" -> authorName)).
      cursor[Comment]().
      collect[List](-1, Cursor.FailOnError[List[Comment]]()))
  }

  override def getTutorAverageRating(tutorName: String): Future[Double] = {
    commentCollection.flatMap(_.find(document("tutorName" -> tutorName)).
      cursor[Comment]().
      collect[List](-1, Cursor.FailOnError[List[Comment]]())).map(a => a.map(_.rating).sum / a.size)
  }

}
