package ru.tinkoff.denis
package billing.rdbms

import scala.concurrent.Future

trait AccountQueryRepository {
  def createNewAccount(login: String, name: String, sum: Int = 0): Future[Int]
  def updateAccount(login: String, sum: Int): Future[Int]
  def updateAccountByName(name: String, sum: Int): Future[Int]
  def deleteAccount(login: String): Future[Int]
}
