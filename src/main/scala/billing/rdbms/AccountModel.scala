package ru.tinkoff.denis
package billing.rdbms

import ru.tinkoff.denis.billing.model.Account
import slick.jdbc.JdbcProfile
import slick.lifted.ProvenShape

class AccountModel(val dbProfile: JdbcProfile) {
  import dbProfile.api._

  class AccountTable(tag: Tag) extends Table[Account](tag, "accounts") {

    def id: Rep[Int] = column("id", O.PrimaryKey, O.AutoInc)

    def login: Rep[String] = column("user_name")

    def name: Rep[String] = column("name")

    def sum: Rep[Int] = column("sum")

    override def * : ProvenShape[Account] = (id, login, name, sum).mapTo[Account]
  }

  val AllAccounts = TableQuery[AccountTable]
}

