package ru.tinkoff.denis
package billing.rdbms

import billing.model.Account
import exceptions.NotEnoughMoneyOnAccountException

import ru.tinkoff.denis.rdbms.rdbms.DIO
import slick.jdbc.JdbcBackend

import scala.concurrent.{ExecutionContext, Future}

class AccountQueryRepositoryImpl(accountModel: AccountModel, db: JdbcBackend#Database)(implicit ec: ExecutionContext) extends AccountQueryRepository {
  import accountModel.AllAccounts
  import accountModel.dbProfile.api._

  def createNewAccount(login: String, name: String, sum: Int = 0): Future[Int] =
    db.run(
      (AllAccounts returning AllAccounts.map(_.id)) += Account(-1, login, name, sum)
    )


  def updateAccount(login: String, sum: Int): Future[Int] = {
    val targetAccount = AllAccounts.filter(_.login === login).map(_.sum)
    val action = for {
      sumOption <- targetAccount.result.headOption
      updateAction = sumOption.map(s => if (s + sum >= 0) targetAccount.update(s + sum) else throw NotEnoughMoneyOnAccountException(login))
      affected <- updateAction.getOrElse(DIO.successful(0))
    } yield affected
    db.run(action)
  }

  def updateAccountByName(name: String, sum: Int): Future[Int] = {
    val targetAccount = AllAccounts.filter(_.name === name).map(_.sum)
    val action = for {
      sumOption <- targetAccount.result.headOption
      updateAction = sumOption.map(s => if (s + sum >= 0) targetAccount.update(s + sum) else throw NotEnoughMoneyOnAccountException(name))
      affected <- updateAction.getOrElse(DIO.successful(0))
    } yield affected
    db.run(action)
  }

  def deleteAccount(login: String): Future[Int] =
    db.run(
      AllAccounts
        .filter(_.login === login)
        .delete
    )

}