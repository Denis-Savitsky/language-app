package ru.tinkoff.denis
package billing.service

import billing.rdbms.AccountQueryRepository

import scala.concurrent.{ExecutionContext, Future}

class BillingServiceImpl(accountQueryRepository: AccountQueryRepository)(implicit val ec: ExecutionContext) extends BillingService {

  override def transferMoney(nameFrom: String, nameTo: String, sum: Int): Future[Int] = {
    accountQueryRepository.updateAccountByName(nameFrom, -sum).flatMap { _ =>
      accountQueryRepository.updateAccountByName(nameTo, sum)
    }
  }

  override def createNewAccount(login: String, name: String): Future[Int] =
    accountQueryRepository.createNewAccount(login, name)

  override def updateAccount(login: String, sum: Int): Future[Int] =
    accountQueryRepository.updateAccount(login, sum)

  override def deleteAccount(login: String): Future[Int] =
    accountQueryRepository.deleteAccount(login)
}
