package ru.tinkoff.denis
package billing.service

import scala.concurrent.Future

trait BillingService {
  def transferMoney(nameFrom: String, nameTo: String, sum: Int): Future[Int]
  def createNewAccount(login: String, name: String): Future[Int]
  def updateAccount(login: String, sum: Int): Future[Int]
  def deleteAccount(login: String): Future[Int]
}
