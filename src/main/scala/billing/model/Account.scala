package ru.tinkoff.denis
package billing.model


case class Account(id: Int,
                   login: String,
                   name: String,
                   sum: Int)