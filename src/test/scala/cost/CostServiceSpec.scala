package ru.tinkoff.denis
package cost

import tutor.model.Tutor

import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper

import scala.concurrent.ExecutionContext.Implicits.global

class CostServiceSpec extends AnyFunSpec {



  describe("Cost service") {
    it("should return correct cost of lesson") {
      val tutor = Tutor(1, "Marie Antoinette ", "English", "Marie the best", "Marie1789@mail.fr", 2)
      val costService = new CostServiceImpl
      costService.calculateCostOfLesson(tutor).map {
        result => result shouldBe 200
      }
    }

    it("should return correct cost of lesson if native") {
      val tutor = Tutor(1, "Marie Antoinette ", "French", "Marie the best", "Marie1789@mail.fr", 9, isNative = true)
      val costService = new CostServiceImpl
      costService.calculateCostOfLesson(tutor).map {
        result => result shouldBe 1800
      }
    }
  }

}
