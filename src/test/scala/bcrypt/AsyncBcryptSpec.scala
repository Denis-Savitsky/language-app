package ru.tinkoff.denis
package bcrypt

import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper

import scala.concurrent.ExecutionContext.Implicits.global

class AsyncBcryptSpec extends AnyFunSpec {

  describe("Bcrypt service") {
    it(" should validate hashed password by correct password") {
      val bcrypt = new AsyncBcryptImpl
      val b = bcrypt.hash("password").flatMap(pass => bcrypt.verify("password", pass)) map {
        result => result shouldBe true
      }
    }

    it(" should not validate hashed password by incorrect password") {
      val bcrypt = new AsyncBcryptImpl
      val b = bcrypt.hash("password").flatMap(pass => bcrypt.verify("passwordWrong", pass)) map {
        result => result shouldBe false
      }
    }

  }

}
