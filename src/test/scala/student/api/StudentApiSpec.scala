package ru.tinkoff.denis
package student.api

import exceptions.{AppExceptionHandler, AppointmentNotExistsException, TutorNotFoundException}
import login.service.LoginService
import student.service.StudentService

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalamock.scalatest.MockFactory
import org.scalatest.funspec.AnyFunSpec
import appointments.model.Appointment

import ru.tinkoff.denis.comment.model.Comment

import scala.concurrent.Future



class StudentApiSpec extends AnyFunSpec with ScalatestRouteTest with MockFactory {

  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

  val mockStudentService: StudentService = mock[StudentService]
  val mockLoginService: LoginService = mock[LoginService]

  def route: Route = Route.seal(
    new StudentApi(mockStudentService, mockLoginService).route
  )(exceptionHandler = AppExceptionHandler.exceptionHandler)

  describe("DELETE /students?login=student&pass=password") {
    it("deletes student") {

      (mockLoginService.verifyCredentials _)
        .expects("student", "password")
        .returns(Future.successful(true))

      (mockStudentService.deleteStudent _)
        .expects("student")
        .returns(Future.successful(1))

      Delete("/students?login=student&password=password") ~> route ~> check {
        assert(status == StatusCodes.OK)
      }
    }
  }

  describe("POST /students/update/password?login=student&password=password&newPassword=newPassword") {
    it("updates password") {

      (mockLoginService.verifyCredentials _)
        .expects("student", "password")
        .returns(Future.successful(true))

      (mockStudentService.updatePassword _)
        .expects("student", "newPassword")
        .returns(Future.successful(1))

      Post("/students/update/password?login=student&password=password&newPassword=newPassword") ~> route ~> check {
        assert(status == StatusCodes.OK)
      }
    }
  }

  describe("POST /students/update/email?login=student&password=password&newEmail=email") {
    it("updates email") {

      (mockLoginService.verifyCredentials _)
        .expects("student", "password")
        .returns(Future.successful(true))

      (mockStudentService.updateEmail _)
        .expects("student", "email")
        .returns(Future.successful(1))

      Post("/students/update/email?login=student&password=password&newEmail=email") ~> route ~> check {
        assert(status == StatusCodes.OK)
      }
    }
  }

  describe("POST /students/appointment?login=student&password=password&tutorName=name&time=16.05.2021") {
    it("creates appointment") {

      (mockLoginService.verifyCredentials _)
        .expects("student", "password")
        .returns(Future.successful(true))

      (mockStudentService.makeAppointment _)
        .expects("student", "name", "16.05.2021")
        .returns(Future.successful(1))

      Post("/students/appointment?login=student&password=password&tutorName=name&time=16.05.2021") ~> route ~> check {
        assert(status == StatusCodes.OK)
      }
    }

    it("returns 400 when no tutor found") {

      (mockLoginService.verifyCredentials _)
        .expects("student", "password")
        .returns(Future.successful(true))

      (mockStudentService.makeAppointment _)
        .expects("student", "name", "16.05.2021")
        .throws(TutorNotFoundException("name"))

      Post("/students/appointment?login=student&password=password&tutorName=name&time=16.05.2021") ~> route ~> check {
        assert(status == StatusCodes.BadRequest)
      }
    }
  }

  describe("POST /students/appointment/cancel?login=student&password=password&time=16.05.2021") {
    it("cancels appointment") {

      (mockLoginService.verifyCredentials _)
        .expects("student", "password")
        .returns(Future.successful(true))

      (mockStudentService.cancelAppointment _)
        .expects("student", "16.05.2021")
        .returns(Future.successful(1))

      Post("/students/appointment/cancel?login=student&password=password&time=16.05.2021") ~> route ~> check {
        assert(status == StatusCodes.OK)
      }
    }

    it("returns 400 when no appointment found") {

      (mockLoginService.verifyCredentials _)
        .expects("student", "password")
        .returns(Future.successful(true))

      (mockStudentService.cancelAppointment _)
        .expects("student", "16.05.2021")
        .throws(AppointmentNotExistsException("student", "16.05.2021"))

      Post("/students/appointment/cancel?login=student&password=password&tutorName=name&time=16.05.2021") ~> route ~> check {
        assert(status == StatusCodes.BadRequest)
      }
    }

    it("returns 400 when found appointment but no tutor found") {

      (mockLoginService.verifyCredentials _)
        .expects("student", "password")
        .returns(Future.successful(true))

      (mockStudentService.cancelAppointment _)
        .expects("student", "16.05.2021")
        .throws(TutorNotFoundException("name"))

      Post("/students/appointment/cancel?login=student&password=password&tutorName=name&time=16.05.2021") ~> route ~> check {
        assert(status == StatusCodes.BadRequest)
      }
    }
  }

  describe("GET /students/appointment?login=student&password=password") {
    it("gets list of appointments") {

      val appointment = Appointment("studentName", "TutorName", "16.05.2021")

      (mockLoginService.verifyCredentials _)
        .expects("student", "password")
        .returns(Future.successful(true))

      (mockStudentService.getListOfAppointments _)
        .expects("student")
        .returns(Future.successful(List(appointment)))

      Get("/students/appointment?login=student&password=password") ~> route ~> check {
        assert(status == StatusCodes.OK)
      }
    }

    it("returns 403 when invalid credentials") {

      (mockLoginService.verifyCredentials _)
        .expects("student", "password")
        .returns(Future.successful(false))

      Get("/students/appointment?login=student&password=password") ~> route ~> check {
        assert(status == StatusCodes.Forbidden)
      }
    }
  }

  describe("POST /students/balance?login=student&password=password&sum=50") {
    it("deposits balance") {

      (mockLoginService.verifyCredentials _)
        .expects("student", "password")
        .returns(Future.successful(true))

      (mockStudentService.depositBalance _)
        .expects("student", 50)
        .returns(Future.successful(1))

      Post("/students/balance?login=student&password=password&sum=50") ~> route ~> check {
        assert(status == StatusCodes.OK)
      }
    }
  }

  describe("GET /students/comments/Martha Kent?login=student&password=password") {
    it("gets comments") {
      val commentOne = Comment("Martha Kent", "Awesome teacher!", 5, "Mone")
      val commentTwo = Comment("Martha Kent", "Helped me to learn english!", 5, "George")
      val commentThree = Comment("Martha Kent", "Didn't find the common language", 3, "Anthony")

      val comments = List(commentOne, commentTwo, commentThree)

      (mockLoginService.verifyCredentials _)
        .expects("student", "password")
        .returns(Future.successful(true))

      (mockStudentService.getAllCommentsForTutor _)
        .expects("Martha_Kent")
        .returns(Future.successful(comments))

      Get("/students/comments/Martha_Kent?login=student&password=password") ~> route ~> check {
        assert(status == StatusCodes.OK)
        assert(responseAs[List[Comment]].contains(commentTwo))
      }
    }
  }


}
