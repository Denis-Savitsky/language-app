package ru.tinkoff.denis
package student.rdbms


import org.scalactic.source
import org.scalatest.compatible
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.funsuite.AsyncFunSuite
import ru.tinkoff.denis.student.model.Student
import slick.dbio.{DBIOAction, Effect, NoStream}
import slick.jdbc.{H2Profile, JdbcBackend}
import slick.jdbc.H2Profile.api._
import slick.jdbc.JdbcBackend.Database

import java.util.UUID
import scala.concurrent.Future

class StudentDataBaseSuite extends AsyncFunSuite {

  protected val studentModel = new StudentModel(H2Profile)
  protected val db: JdbcBackend.Database = Database.forConfig("h2mem1")
  protected val studentQueryRepository = new StudentQueryRepositoryImpl(studentModel, db)
  protected val allStudents = studentModel.AllStudents

  protected def test[R, S <: NoStream, E <: Effect](testName: String)
                                                   (testFun: => Future[compatible.Assertion])
                                                   (implicit pos: source.Position): Unit = {
    super.test(testName) {
      db.run(
        initSchema
          .andThen(allStudents ++= SampleStudents)
      )
        .flatMap(_ => testFun)
        .andThen { case _ => db.close() }
    }
  }

  protected val initSchema =
    allStudents.schema.create


  val SampleStudents = Seq(
    Student(1, "Eugene Bohr", "eb@email.com", "EB1902", "Advanced"),
    Student(2, "Martha Kent", "mk@email.com", "MK1938", "Fluent"),
  )
}