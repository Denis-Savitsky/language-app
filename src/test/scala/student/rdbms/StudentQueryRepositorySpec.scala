package ru.tinkoff.denis
package student.rdbms

import org.scalatest.matchers.should.Matchers
import ru.tinkoff.denis.student.model.Student
import ru.tinkoff.denis.wirings.BaseModule.ac.dispatcher



class StudentQueryRepositorySpec extends StudentDataBaseSuite with Matchers {

  test("student query repository") {
    studentQueryRepository.getAllStudents.map { result =>
      result shouldBe SampleStudents
    }
  }

  test("findStudent should return student") {
    studentQueryRepository.findStudent(1).flatMap {result =>
      result shouldBe SampleStudents.headOption}
  }

  test("findStudentByLogin should return student") {
    studentQueryRepository.findStudentByLogin("EB1902").flatMap {
      case Some(student) => student shouldBe SampleStudents.head
      }
  }

  test("findStudentByLogin should not return student by invalid login") {
    studentQueryRepository.findStudentByLogin("SAMPLE").map { result =>
      result shouldBe None
    }
  }

  test("findStudentByName should return student") {
    studentQueryRepository.findStudentByName("Martha Kent").flatMap {
      case Some(student) => SampleStudents should contain (student)
    }
  }

  test("findStudentByName should not return student by invalid name") {
    studentQueryRepository.findStudentByName("SAMPLE").map { result =>
      result shouldBe None
    }
  }

  test("addStudent should add student") {
    val student = Student(3, "Bob Marley", "BM@mail.com", "BM420", "Advanced")
    studentQueryRepository.addStudent(student).flatMap { _ =>
      studentQueryRepository.getAllStudents.map { result =>
        result.size shouldBe SampleStudents.size + 1
      }
    }
  }

  test("deleteStudent should delete student") {
    studentQueryRepository.deleteStudent("MK1938").flatMap { _ =>
      studentQueryRepository.getAllStudents.map { result =>
        result.size shouldBe SampleStudents.size - 1
      }
    }
  }

    //      db.run(
    //        initSchema
    //          .andThen(allStudents ++= SampleStudents)
    //      ).flatMap { _ =>
    //        studentQueryRepository.findStudentByLogin("EB1902").flatMap {
    //          case Some(student) => Future(student).map { result =>
    //            result shouldBe SampleStudents.head
    //          }
    //        }
    //      }.andThen(_ => db.close())
    //    }
//
//    it("findStudent should return student") {
//      db.run(
//        initSchema
//          .andThen(allStudents ++= SampleStudents)
//      ).flatMap { _ =>
//        studentQueryRepository.findStudent(1).flatMap {
//          case Some(student) => Future(student).map { result =>
//            result shouldBe SampleStudents.head
//          }
//        }
//      }.andThen(_ => db.close())
//    }
//
//    it("findStudentByLogin should return student") {
//      db.run(
//        initSchema
//          .andThen(allStudents ++= SampleStudents)
//      ).flatMap { _ =>
//        studentQueryRepository.findStudentByLogin("EB1902").flatMap {
//          case Some(student) => Future(student).map { result =>
//            result shouldBe SampleStudents.head
//          }
//        }
//      }.andThen(_ => db.close())
//    }
//
//    it("findStudentByLogin should not return student by non-existing login") {
//      db.run(
//        initSchema
//          .andThen(allStudents ++= SampleStudents)
//      ).flatMap { _ =>
//        studentQueryRepository.findStudentByLogin("SAMPLE LOGIN").flatMap {
//          case Some(student) => Future(student).map { result =>
//            result shouldBe SampleStudents
//          }
//        }
//      }
//    }
//
//    it("findStudentByName should return student") {
//      db.run(
//        initSchema
//          .andThen(allStudents ++= SampleStudents)
//      ).flatMap { _ =>
//        studentQueryRepository.findStudentByLogin("EB1902").flatMap {
//          case Some(student) => Future(student).map { result =>
//            result shouldBe SampleStudents.head
//          }
//        }
//      }.andThen(_ => db.close())
//    }
//
//    it("findStudentByName should not return student by non-existing name") {
//      db.run(
//        initSchema
//          .andThen(allStudents ++= SampleStudents)
//      ).flatMap { _ =>
//        studentQueryRepository.findStudentByName("SAMPLE Name").flatMap {
//          case Some(student) => Future(student).map { result =>
//            result shouldBe SampleStudents.head
//          }
//        }
//      }.andThen(_ => db.close())
//    }
//  }
//

}
