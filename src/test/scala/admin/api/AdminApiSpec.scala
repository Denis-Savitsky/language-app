package ru.tinkoff.denis
package admin.api


import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalamock.scalatest.MockFactory
import org.scalatest.funspec.AnyFunSpec
import ru.tinkoff.denis.admin.service.{AdminAuthorizationService, AdminService}
import ru.tinkoff.denis.appointments.model.Appointment
import ru.tinkoff.denis.comment.model.Comment
import ru.tinkoff.denis.exceptions.AppExceptionHandler

import scala.concurrent.Future


class AdminApiSpec extends AnyFunSpec with ScalatestRouteTest with MockFactory {

  import de.heikoseeberger.akkahttpcirce.FailFastCirceSupport._

  val mockAdminService: AdminService = mock[AdminService]
  val mockAdminAuthorizationService: AdminAuthorizationService = mock[AdminAuthorizationService]

  val appointment: Appointment = Appointment("name2", "name1", "16.05.2021")
  val appointmentTwo: Appointment = Appointment("tutorName2", "StudentName1", "16.05.2021")

  def route: Route = Route.seal(
    new AdminApi(mockAdminService, mockAdminAuthorizationService).route
  )(exceptionHandler = AppExceptionHandler.exceptionHandler)

  describe("POST /admin/cancel?pass=password&studentName=name1&tutorName=name2&time=16.05.2021") {
    it("cancels appointment of student and tutor") {
      (mockAdminAuthorizationService.authorize _)
        .expects("password")
        .returns(Future.successful(true))

      (mockAdminService.cancelAppointment _)
        .expects("name1", "name2", "16.05.2021")
        .returns(Future.successful(Some(appointment)))

      Post("/admin/cancel?pass=password&studentName=name1&tutorName=name2&time=16.05.2021") ~> route ~> check {
        assert(status == StatusCodes.OK)
        assert(responseAs[Option[Appointment]].contains(appointment))
      }
    }
  }


  describe("POST /admin/cancel?pass=password&studentName=name1&time=16.05.2021") {
    it("cancels appointment of student") {
      (mockAdminAuthorizationService.authorize _)
        .expects("password")
        .returns(Future.successful(true))

      (mockAdminService.cancelAppointmentOfStudent _)
        .expects("name1", "16.05.2021")
        .returns(Future.successful(Some(appointment)))

      Post("/admin/cancel?pass=password&studentName=name1&time=16.05.2021") ~> route ~> check {
        assert(status == StatusCodes.OK)
        assert(responseAs[Option[Appointment]].contains(appointment))
      }
    }
  }

  describe("POST /admin/cancel?pass=password&tutorName=name1&time=16.05.2021") {
    it("cancels appointment of tutor") {
      (mockAdminAuthorizationService.authorize _)
        .expects("password")
        .returns(Future.successful(true))

      (mockAdminService.cancelAppointmentOfTutor _)
        .expects("name1", "16.05.2021")
        .returns(Future.successful(Some(appointment)))

      Post("/admin/cancel?pass=password&tutorName=name1&time=16.05.2021") ~> route ~> check {
        assert(status == StatusCodes.OK)
        assert(responseAs[Option[Appointment]].contains(appointment))
      }
    }
  }

  describe("POST /admin/delete/student/name1?pass=password") {
    it("deletes student") {
      (mockAdminAuthorizationService.authorize _)
        .expects("password")
        .returns(Future.successful(true))

      (mockAdminService.deleteStudent _)
        .expects("name1")
        .returns()

      Delete("/admin/delete/student/name1?pass=password") ~> route ~> check {
        assert(status == StatusCodes.OK)
      }
    }
  }

  describe("POST /admin/delete/tutor/name1?pass=password") {
    it("deletes tutor") {
      (mockAdminAuthorizationService.authorize _)
        .expects("password")
        .returns(Future.successful(true))

      (mockAdminService.deleteTutor _)
        .expects("name1")
        .returns()

      Delete("/admin/delete/tutor/name1?pass=password") ~> route ~> check {
        assert(status == StatusCodes.OK)
      }
    }
  }

  describe("GET /admin/appointments?pass=password") {
    it("list all appointment") {
      (mockAdminAuthorizationService.authorize _)
        .expects("password")
        .returns(Future.successful(true))

      (mockAdminService.listAllAppointments _)
        .expects()
        .returns(Future.successful(List(appointment, appointmentTwo)))

      Get("/admin/appointments?pass=password") ~> route ~> check {
        assert(status == StatusCodes.OK)
        assert(responseAs[List[Appointment]].contains(appointment))
        assert(responseAs[List[Appointment]].contains(appointmentTwo))
      }
    }
  }

  describe("GET /admin/appointments/tutors/tutorName?pass=password") {
    it("list all appointment of tutor") {
      (mockAdminAuthorizationService.authorize _)
        .expects("password")
        .returns(Future.successful(true))

      (mockAdminService.listAppointmentsOfTutor _)
        .expects("tutorName")
        .returns(Future.successful(List(appointment, appointmentTwo)))

      Get("/admin/appointments/tutors/tutorName?pass=password") ~> route ~> check {
        assert(status == StatusCodes.OK)
        assert(responseAs[List[Appointment]].contains(appointment))
        assert(responseAs[List[Appointment]].contains(appointmentTwo))
      }
    }
  }

  describe("GET /admin/appointments/students/studentName?pass=password") {
    it("list all appointment of student") {
      (mockAdminAuthorizationService.authorize _)
        .expects("password")
        .returns(Future.successful(true))

      (mockAdminService.listAppointmentsOfStudent _)
        .expects("studentName")
        .returns(Future.successful(List(appointment, appointmentTwo)))

      Get("/admin/appointments/students/studentName?pass=password") ~> route ~> check {
        assert(status == StatusCodes.OK)
        assert(responseAs[List[Appointment]].contains(appointment))
        assert(responseAs[List[Appointment]].contains(appointmentTwo))
      }
    }
  }

  describe("GET /admin/comments?pass=password") {
    it("list all comments") {

      val commentOne = Comment("Martha Kent", "Awesome teacher!", 5, "Mone")
      val commentTwo = Comment("Martha Kent", "Helped me to learn english!", 5, "George")
      val commentThree = Comment("Martha Kent", "Didn't find the common language", 3, "Anthony")

      val comments = List(commentOne, commentTwo, commentThree)

      (mockAdminAuthorizationService.authorize _)
        .expects("password")
        .returns(Future.successful(true))

      (mockAdminService.getAllComments _)
        .expects()
        .returns(Future.successful(comments))

      Get("/admin/comments?pass=password") ~> route ~> check {
        assert(status == StatusCodes.OK)
        assert(responseAs[List[Comment]].contains(commentTwo))
        assert(responseAs[List[Comment]].contains(commentOne))
      }
    }
  }



}
