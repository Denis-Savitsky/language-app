addSbtPlugin("org.jetbrains" % "sbt-ide-settings" % "1.1.0")
addSbtPlugin("com.permutive" % "sbt-liquibase" % "1.2.0")
addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.7.6")